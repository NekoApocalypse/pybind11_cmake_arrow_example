#include <Python.h>
#include <pybind11/pybind11.h>

#include <arrow/table.h>
#include <arrow/api.h>
#include <arrow/io/api.h>
#include <arrow/ipc/api.h>
#include <arrow/python/pyarrow.h>

#include <parquet/arrow/reader.h>
#include <parquet/arrow/writer.h>
#include <parquet/exception.h>

#include <string>
#include <iostream>

namespace py = pybind11;

namespace {

std::shared_ptr<arrow::Table> load_table_from_parquet(std::string file_path) {
     std::shared_ptr<arrow::Table> table;
    std::shared_ptr<arrow::io::ReadableFile> infile;
    PARQUET_ASSIGN_OR_THROW(infile,
                            arrow::io::ReadableFile::Open(file_path, arrow::default_memory_pool()));

    std::unique_ptr<parquet::arrow::FileReader> reader;
    PARQUET_THROW_NOT_OK(parquet::arrow::OpenFile(infile, arrow::default_memory_pool(), &reader));
    PARQUET_THROW_NOT_OK(reader->ReadTable(&table));
    return table; 
}

// NOTE: There is also a PYBIND11_TYPE_CASTER solution
// may help with pyi stub generation
// https://stackoverflow.com/questions/70620554/pyarrow-table-to-pyobject-via-pybind11

void inspect_table(py::object py_table) {
  auto status = arrow::py::unwrap_table(py_table.ptr());
  if (!status.ok()) {
    throw std::runtime_error("Failed to unwrap table");
  }
  auto table = status.ValueOrDie();
  std::cout << "Table has " << table->num_rows() << " rows and "
            << table->num_columns() << " columns" << std::endl;
  return;
}

py::object example_load_parquet(std::string file_path) {
  PyObject* object = arrow::py::wrap_table(load_table_from_parquet(file_path));
  return py::reinterpret_steal<py::object>(object);
}

}  // namespace

PYBIND11_MODULE(arrow_pybind_example, m) {
  arrow::py::import_pyarrow();
  m.def("example_load_parquet", &example_load_parquet,
        R"pbdoc(
        Loads a Parquet file as a PyArrow table.
    )pbdoc");
  m.def("inspect_table", &inspect_table,
        R"pbdoc(
        Inspect a PyArrow table.
    )pbdoc");
  m.attr("__version__") = "0.1.0";
}
