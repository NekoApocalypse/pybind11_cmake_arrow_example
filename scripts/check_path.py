import pyarrow as pa
from rich import print
import pybind11


def configure_pyarrow():
    pa.create_library_symlinks()


def main():
    include_dir = pa.get_include()
    print(include_dir)
    lib_dir = pa.get_library_dirs()
    print(lib_dir)
    libs = pa.get_libraries()
    print(libs)
    print(pybind11.get_cmake_dir())


if __name__ == "__main__":
    main()
