import os, sys
import pyarrow as pa
import polars as pl

# sys.path.append(os.path.join(os.path.dirname(__file__), "../build"))

import arrow_pybind_example


def main():
    print(arrow_pybind_example.__version__)
    y = arrow_pybind_example.example_load_parquet("data/roundtrips.parquet")
    arrow_pybind_example.inspect_table(y)


if __name__ == "__main__":
    main()